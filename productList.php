<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/listStyles.css">
  <link rel="icon" href="img/favicon.ico">
  <title>Product List</title>

</head>

<body>
  <form method="post">
    <div id="header">
      <div id="logo">
        <p>Product List</p>
      </div>
      <div>
        <input type="submit" name="submitButton" value="Delete" id="submitButton">
      </div>
    </div>

    <hr>

    <div class="container">

      <?php
    //Include Database Class
    include("php/dbClass.php");
    //Include variables required for connection
    include("php/connectionString.php");
    //Create Database connection
    $db = new Database($host, $username, $password, $databaseName);

    //Select all data from database "products" table
    $result = $db->query("SELECT * FROM products");

    //compare number of data rows with counter
    //data from each existing row, filled in to the separate div ("square")
    if ($result->num_rows > 0) {
    $i=0;
    while($row = mysqli_fetch_assoc($result)) {
    ?>

      <div class="square">
        <div class="checkbox">
          <input class="checkboxInput" type="checkbox" name="checkedProduct[]" value="<?php echo $db->SecureInput($row["id"]); ?>">
        </div>
        <div class="productInfo">
          <div>

            <p>Sku is: <?php echo $db->SecureInput($row["sku"]); ?></p>
            <p>Name is: <?php echo $db->SecureInput($row["name"]); ?></p>
            <p>Price is: <?php echo $db->SecureInput($row["price"]); ?> $</p>
          </div>
          <div class="<?php echo $db->SecureInput($row["type"]); ?>">
            <p class="dvdInfo">Size: <?php echo $db->SecureInput($row["size"]); ?> MB</p>
            <p class="bookInfo">Weight: <?php echo $db->SecureInput($row["weight"]); ?> KG</p>
            <p class="furnitureInfo">Dimension: <?php echo $db->SecureInput($row["height"]); ?>x<?php echo $db->SecureInput($row["width"]); ?>x<?php echo $db->SecureInput($row["length"]); ?></p>
          </div>
        </div>
      </div>

      <?php
    $i++;
      }
    } else {
    echo "Can't find any product.";
    }
    ?>
    </div>
  </form>
</body>
</html>

<?php
//Select "Apply" button
if(isset($_POST["submitButton"])){
//Check for selected products
foreach($_POST["checkedProduct"] as $selected) {
//If "Apply" button trigered, delete selected products
$db->query("DELETE FROM products WHERE id = '$selected'");
}
//Is used to refresh page
echo "<meta http-equiv='refresh' content='0'>";
}
?>
