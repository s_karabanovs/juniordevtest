<?php

//Include Database Class
include("php/dbClass.php");
//Include variables required for connection
include("php/connectionString.php");
//Create Database connection
$db = new Database($host, $username, $password, $databaseName);

//Variables to store data for the querys
$keyString = "";
$valueString = "";

//Select "Save" button
if(isset($_POST["submitButton"])){
//Gather data from input fields
  foreach ($_POST as $key => $value) {
    if ($value == ""){
      continue;
    };
    if ($key == "submitButton"){
      continue;
    };
    if ($_POST["type"] == "0"){
      continue;
    };

    //Escapes special characters in a string
    $values = $db->SecureInput($value);
    //Storing gathered data
    $keyString .= "`$key`, ";
    $valueString .= "'$values', ";

  };
  //Making data suitable for query
  $keyString = substr($keyString, 0, -2);
  $valueString = substr($valueString, 0, -2);

  //Passing data to database table
  $db->query("INSERT INTO `products` ($keyString) VALUES ($valueString)");
  //prevent posted data duplecation on refresh
  header("Location: productAdd.php");
  exit;
};

 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/addStyles.css">
  <link rel="icon" href="img/favicon.ico">
  <title>Product Add</title>
</head>

<body>

  <form id="myForm" method="post">

    <div id="header">
      <div id="logo">
        <p>Product Add</p>
      </div>
      <div>
        <input type="submit" name="submitButton" value="Save" id="submitButton">
      </div>
    </div>

    <hr>

    <div class="wrapper">
      <div class="formElement">
        <label for="sku">SKU</label>
        <input class="must textInput noPaste" type="text" name="sku" id="sku">
      </div>

      <div class="formElement">
        <label for="name">Name</label>
        <input class="must textInput noPaste" type="text" name="name" id="name">
      </div>

      <div class="formElement">
        <label for="price">Price</label>
        <input class="must priceWeight noPaste" type="text" name="price" id="price">
      </div>
    </div>

    <div class="wrapper selector">

      <div class="formElement">
        <label for="viewSelector">Type Switcher</label>
        <select name="type" id="viewSelector">
          <option value="0">Type Switcher</option>
          <option value="Dvd">DVD-disc</option>
          <option value="Book">Book</option>
          <option value="Furniture">Furniture</option>
        </select>
      </div>

      <div class="find">

        <div class="hide formElement" id="option1">
          <label for="size">Size</label>
          <!-- pattern="[0-9]*" used to open numeric keyboard on mobile device-->
          <input class="must a sizeDimension noPaste" type="text" name="size" placeholder="Enter value in MB" id="size">
          <div class="selectedProductText">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          </div>
        </div>

        <div class="hide formElement" id="option2">
          <label for="weight">Weight</label>
          <input class="must a priceWeight noPaste" type="text" name="weight" placeholder="Enter value in kg" id="weight">
          <div class="selectedProductText">
            <p>Pretium quam vulputate dignissim suspendisse. Tortor at risus viverra adipiscing at in tellus integer.</p>
          </div>
        </div>

        <div class="hide" id="option3">
          <div class="formElement">
            <label for="height">Height</label>
            <input class="must a sizeDimension noPaste" type="text" name="height" placeholder="Enter value in cm" id="height">
          </div>

          <div class="formElement">
            <label for="width">Width</label>
            <input class="must a sizeDimension noPaste" type="text" name="width" placeholder="Enter value in cm" id="width">
          </div>

          <div class="formElement">
            <label for="lenght">Length</label>
            <input class="must a sizeDimension noPaste" type="text" name="length" placeholder="Enter value in cm" id="lenght">
          </div>
          <div class="selectedProductText">
            <p>Sit amet consectetur adipiscing elit duis tristique. Velit scelerisque in dictum non consectetur a erat.</p>
          </div>
        </div>

      </div>
    </div>
  </form>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="js/productAdd.js" charset="utf-8"></script>

</body>
</html>
