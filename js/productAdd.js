
/**SELECTOR BLOCK
When the document is ready
Define a Map of keys to values
*/
$(document).ready(function() {
  $.selectorOptions = {
    "0": $([]),
    "Dvd": $("#option1"),
    "Book": $("#option2"),
    "Furniture": $("#option3")
  };


  //When element value is changed
  $("#viewSelector").change(function() {
    // Hide every selector option
    $.each($.selectorOptions, function() {
      this.hide();
    });
    //Remove class, which gives importance to fill selectoin values
    $(".must.a").removeClass("must");
    // show selected option and add to it importance to be filled or changed(selector)
    $.selectorOptions[$(this).val()].show();
    $(".find").find(".a:visible").addClass("must");

    if ($("#viewSelector").val() == 0) {
      $(".a").addClass("must");
    };
  });
});


//INPUT FIELD VALUE CHECK BLOCK
//Select SKU and Name input fields
//"input" event to detect any change
//Define rule, for input charcters
//Character rule = alphabetic characters,numbers,'()'
$(".textInput").on("input", function(e) {
  this.value = this.value.match(/^([a-zA-Z0-9()]+\s?)*/g);
});
//Limit input value length
$(".textInput").keypress(function(e) {
  if ($(this).val().length >= 16) {
    e.preventDefault();
  }
  });

//Select Price input field
//Character rule = number with 2 decimal places
$("#price").on("input", function() {
  this.value = this.value.match(/^\d{0,8}\.?\d{0,2}/);
});


//Select Weight input field
//Character rule = number with 3 decimal places
$("#weight").on("input", function() {
  this.value = this.value.match(/^\d{0,7}\.?\d{0,3}/);
});

//Select Size and Furniture input fields
//Character rule = whole number
$(".sizeDimension").on("input", function() {
  this.value = this.value.match(/^\d{0,6}/);
});


//INPUT FIELDS RESTRICTIONS
//Restrict cut, copy and paste for all input fields
$(".noPaste").bind("cut copy paste", function(e) {
  e.preventDefault();
});


//SUBMIT BUTTON BLOCK
//When the button is clicked
$("#submitButton").click(function() {
  //Check if all fields are filled
  if ($(".must").filter(function() {
      //return all inputs with class "must" that are empty.
      return this.value === '';
    }).length === 0) { //false
    //Restore submiting functionality
    $("#myForm").unbind("submit").submit();
    alert("New product is created.");
  } else {
    //Cancel submiting functionality
    $("#myForm").submit(function(e) {
      e.preventDefault();
    });
    alert("Error. Please fill all fields.");
  };
});
