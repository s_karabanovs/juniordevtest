<?php

Class Database {

  private $host;
  private $username;
  private $password;
  private $databaseName;
  public $link;
  public $result;

  //When new object is created, this func will be atomatically called
  public function __construct($host, $username, $password, $databaseName){
      $this->host = $host;
      $this->username = $username;
      $this->password = $password;
      $this->databaseName = $databaseName;

      $this->link = new mysqli($this->host, $this->username, $this->password, $this->databaseName);

          // Check connection
          if (mysqli_connect_error()){
            die ("Database connection error");
          }
          // else statement for testing connection
          // else {
          //   echo "u are connected!";
          // }
        }

        //Querys such: select, update, insert, delete can be run
        public function query($query){
        	$this->result = $this->link->query($query);
        	return $this->result;
        }

        //return array with all selected rows
        public function fetch(){
        	if(!$this->result){
        		return "no results";
        	}
        	while($row = $this->result->fetch_assoc()){
        		$rows[] = $row;
        	}
        	return $rows;
        }

        //Escapes special characters in a string
      	public function SecureInput($safety) {
          return $this->link -> real_escape_string($safety);
      	}
      }
 ?>
